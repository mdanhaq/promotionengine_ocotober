﻿using PromotionEngine.SKU_IDs;
using System;

namespace PromotionEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            int totalValue = 0;

            Console.WriteLine("Enter only digit as a number");

            string userInputForA = string.Empty;
            while (Validator.FieldIsEmpty(userInputForA) || !(Validator.InputIsDigit(userInputForA)))
            {
                Console.Write("\nEnter the number of A (only digit): ");
                userInputForA = Console.ReadLine();
                if (Validator.FieldIsEmpty(userInputForA))
                {
                    Console.WriteLine("\nAvoid to input empty string");
                }
                else if (!(Validator.InputIsDigit(userInputForA)))
                {
                    Console.WriteLine("Please avoid to input characters");
                }
            }
           
            int numberOfSKU_ID_A = Convert.ToInt32(userInputForA);
            SKU_ID_A sKU_ID_A = new SKU_ID_A(numberOfSKU_ID_A);
            totalValue += sKU_ID_A.CalculatingValue();
            
            string userInputForB = string.Empty;
            while (Validator.FieldIsEmpty(userInputForB) || !(Validator.InputIsDigit(userInputForB)))
            {
                Console.Write("\nEnter the number of B (only digit): ");
                userInputForB = Console.ReadLine();
                if (Validator.FieldIsEmpty(userInputForB))
                {
                    Console.WriteLine("\nAvoid to input empty string");
                }
                else if (!(Validator.InputIsDigit(userInputForB)))
                {
                    Console.WriteLine("Please avoid to input characters");
                }

            }

            int numberOfSKU_ID_B = Convert.ToInt32(userInputForB);
            SKU_ID_B sKU_ID_B = new SKU_ID_B(numberOfSKU_ID_B);
            totalValue += sKU_ID_B.CalculatingValue();

            string userInputForC = string.Empty;
            while (Validator.FieldIsEmpty(userInputForC) || !(Validator.InputIsDigit(userInputForC)))
            {
                Console.Write("\nEnter the number of C (only digit): ");
                userInputForC = Console.ReadLine();
                if (Validator.FieldIsEmpty(userInputForC))
                {
                    Console.WriteLine("\nAvoid to input empty string");
                }
                else if (!(Validator.InputIsDigit(userInputForC)))
                {
                    Console.WriteLine("Please avoid to input characters");
                }

            }

            int numberOfSKU_ID_C = Convert.ToInt32(userInputForC);
            
            string userInputForD = string.Empty;
            while (Validator.FieldIsEmpty(userInputForD) || !(Validator.InputIsDigit(userInputForD)))
            {
                Console.Write("\nEnter the number of B (only digit): ");
                userInputForD = Console.ReadLine();
                if (Validator.FieldIsEmpty(userInputForD))
                {
                    Console.WriteLine("\nAvoid to input empty string");
                }
                else if (!(Validator.InputIsDigit(userInputForD)))
                {
                    Console.WriteLine("Please avoid to input characters");
                }

            }

            int numberOfSKU_ID_D = Convert.ToInt32(userInputForD);

            SKU_ID_CD sKU_ID_CD = new SKU_ID_CD(numberOfSKU_ID_C, numberOfSKU_ID_D);
            totalValue += sKU_ID_CD.CalculatingValue();
            Console.Write("\nTotal value is: " + totalValue);

            Console.ReadLine();

        }
    }
}
