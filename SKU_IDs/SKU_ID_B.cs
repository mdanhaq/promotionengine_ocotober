﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromotionEngine.SKU_IDs
{
    public class SKU_ID_B
    {
        private int divisible { get; set; }
        private int remainder { get; set; }
        private int numberOfUnitB { get; set; }
        public SKU_ID_B(int inputB)
        {
            this.numberOfUnitB = inputB;
        }

        public int CalculatingValue()
        {
            this.divisible = numberOfUnitB / 2;
            this.remainder = numberOfUnitB % 2;

            return this.divisible * 45 + this.remainder * 30;
        }
    }
}
