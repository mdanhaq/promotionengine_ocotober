﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PromotionEngine.SKU_IDs
{
    public class SKU_ID_CD 
    {
        private int numberOfUnitC;
        private int numberOfUnitD;
        public SKU_ID_CD(int inputC, int inputD)
        {
            this.numberOfUnitC = inputC;
            this.numberOfUnitD = inputD;
        }

        public int CalculatingValue()
        {
            if (this.numberOfUnitC == this.numberOfUnitD)
                return this.numberOfUnitC * 30;
            else
                return (this.numberOfUnitC * 20 + this.numberOfUnitD * 15);
        }

    }
}
  
